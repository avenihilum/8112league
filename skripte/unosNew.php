<?php
	$liga = $_POST['liga'];
	$prvi = $_POST['1'];
	$drugi = $_POST['2'];
	$treci = $_POST['3'];
	$cetvrti = $_POST['4'];

	include('../connection.php');

	$query1 = mysqli_query($db, "SELECT $liga FROM igraci WHERE ime = '$prvi'");
	$query2 = mysqli_query($db, "SELECT $liga FROM igraci WHERE ime = '$drugi'");
	$query3 = mysqli_query($db, "SELECT $liga FROM igraci WHERE ime = '$treci'");
	$query4 = mysqli_query($db, "SELECT $liga FROM igraci WHERE ime = '$cetvrti'");
	$data1 = mysqli_fetch_assoc($query1);
	$data2 = mysqli_fetch_assoc($query2);
	$data3 = mysqli_fetch_assoc($query3);
	$data4 = mysqli_fetch_assoc($query4);

	echo $prvi. " - " .$data1[$liga]. "<br />";
	echo $drugi. " - " .$data2[$liga]. "<br />";
	echo $treci. " - " .$data3[$liga]. "<br />";
	echo $cetvrti. " - " .$data4[$liga]. "<br />";

	$totalElo = $data1[$liga] + $data2[$liga] + $data3[$liga] + $data4[$liga];

	echo "Total Elo: " .$totalElo. "<br />";

	echo "--------------------------<br />";

	//KALKULACIJE SANSI --------------------------------------------------

	$jedanProcenat = $totalElo/100; //VREDNOST ELA KOJA PREDSTAVLJA 1% SANSE ZA POBEDU

	$sansaZaPobedu1 = $data1[$liga]/$jedanProcenat;  //SANSA ZA POBEDU #1 IGRACA

	if ($sansaZaPobedu1 > 49) {
		$sansaZaPobedu1 = 49;
	};

	$sansaZaPobedu2 = $data2[$liga]/$jedanProcenat; //SANSA ZA POBEDU #2 IGRACA

	if ($sansaZaPobedu2 > 49) {
		$sansaZaPobedu2 = 49;
	};

	$sansaZaPobedu3 = $data3[$liga]/$jedanProcenat; //SANSA ZA POBEDU #3 IGRACA

	if ($sansaZaPobedu3 > 49) {
		$sansaZaPobedu3 = 49;
	};

	$sansaZaPobedu4 = $data4[$liga]/$jedanProcenat; //SANSA ZA POBEDU #4 IGRACA

	if ($sansaZaPobedu4 > 49) {
		$sansaZaPobedu4 = 49;
	};

	echo $prvi. " - sansa za pobedu - " .$sansaZaPobedu1. "<br />" .$drugi. " - sansa za pobedu - " .$sansaZaPobedu2. "<br />" .$treci. " - sansa za pobedu - " .$sansaZaPobedu3. "<br />" .$cetvrti. " - sansa za pobedu - " .$sansaZaPobedu4. "<br />";

	echo "--------------------------<br />";

	//KALKULACIJE ELA --------------------------------------------------

	$K = 100;

	//PRVI

	$prviEloModifier = round(25 - $sansaZaPobedu1, 0, PHP_ROUND_HALF_DOWN);

	echo "prviEloModifier = " .$prviEloModifier. "<br />";

	$one = (1 + abs($prviEloModifier)/$K);

	echo "prviEloFinalK = " .$one. "<br />";

	$one = $prviEloModifier*(1 + abs($prviEloModifier)/$K);

	echo "prviEloFinalChange = " .$one. "<br />";

	$prviEloGain = round(30 + $prviEloModifier*(1 + abs($prviEloModifier)/$K), 0, PHP_ROUND_HALF_DOWN);

	$prviFinalElo = $data1[$liga] + $prviEloGain;

	echo "Prvi Elo Gain: " .$prviEloGain. "<br />";

	echo "Prvi Elo: " .$prviFinalElo. "<br />";

	echo "--------------------------<br />";

	//DRUGI

	$drugiEloModifier = round(25 - $sansaZaPobedu2, 0, PHP_ROUND_HALF_DOWN);

	echo "drugiEloModifier = " .$drugiEloModifier. "<br />";

	$two = (1 + abs($drugiEloModifier)/$K);

	echo "drugiEloFinalK = " .$two. "<br />";

	$two = $drugiEloModifier*(1 + abs($drugiEloModifier)/$K);

	echo "drugiEloFinalChange = " .$two. "<br />";

	$drugiEloGain = round(15 + $drugiEloModifier*(1 + abs($drugiEloModifier)/$K), 0, PHP_ROUND_HALF_DOWN);

	$drugiFinalElo = $data2[$liga] + $drugiEloGain;

	echo "Drugi Elo Gain: " .$drugiEloGain. "<br />";

	echo "Drugi Elo: " .$drugiFinalElo. "<br />";

	echo "--------------------------<br />";

	//TRECI

	$treciEloModifier = round(25 - $sansaZaPobedu3, 0, PHP_ROUND_HALF_DOWN);

	echo "treciEloModifier = " .$treciEloModifier. "<br />";

	$three = (1 + abs($treciEloModifier)/$K);

	echo "treciEloFinalK = " .$three. "<br />";

	$three = $treciEloModifier*(1 + abs($treciEloModifier)/$K);

	echo "treciEloFinalChange = " .$three. "<br />";

	$treciEloLoss = round(15 - $treciEloModifier*(1 + abs($treciEloModifier)/$K), 0, PHP_ROUND_HALF_DOWN);

	$treciFinalElo = $data3[$liga] - $treciEloLoss;

	echo "Treci Elo Loss: " .$treciEloLoss. "<br />";

	echo "Treci Elo: " .$treciFinalElo. "<br />";

	echo "--------------------------<br />";

	//CETVRTI

	$cetvrtiEloModifier = round(25 - $sansaZaPobedu4, 0, PHP_ROUND_HALF_DOWN);

	echo "cetvrtiEloModifier = " .$cetvrtiEloModifier. "<br />";

	$four = (1 + abs($cetvrtiEloModifier)/$K);

	echo "cetvrtiEloFinalK = " .$four. "<br />";

	$four = $cetvrtiEloModifier*(1 + abs($cetvrtiEloModifier)/$K);

	echo "cetvrtiEloFinalChange = " .$four. "<br />";

	$cetvrtiEloLoss = round(30 - $cetvrtiEloModifier*(1 + abs($cetvrtiEloModifier)/$K), 0, PHP_ROUND_HALF_DOWN);

	$cetvrtiFinalElo = $data4[$liga] - $cetvrtiEloLoss;

	echo "Cetvrti Elo Loss: " .$cetvrtiEloLoss. "<br />";

	echo "Cetvrti Elo: " .$cetvrtiFinalElo. "<br />";

	echo "--------------------------<br />";

	//UPDATE --------------------------------------------------

	$updateQuery1 = mysqli_query($db, "UPDATE igraci SET $liga='$prviFinalElo'  WHERE ime='$prvi'");

	$updateQuery2 = mysqli_query($db, "UPDATE igraci SET $liga='$drugiFinalElo' WHERE ime='$drugi'");

	$updateQuery3 = mysqli_query($db, "UPDATE igraci SET $liga='$treciFinalElo' WHERE ime='$treci'");

	$updateQuery4 = mysqli_query($db, "UPDATE igraci SET $liga='$cetvrtiFinalElo' WHERE ime='$cetvrti'");

?>