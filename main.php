<!DOCTYPE html>
<html>
	<head>
		<title>8112</title>
		<link rel="stylesheet" type="text/css" href="css/main.css" />
		<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
	</head>

	<?php include('skripte/loginStatus.php'); ?>

	<body>
		<div id="korisnik">
			<?php
				echo $_SESSION['ime']. "</br>";
				echo "All Pick Elo: " .$_SESSION['allPickElo']. "<br />";
				echo "Locked Pick Elo: " .$_SESSION['lockedPickElo']. "<br />";
				echo "No Powerups Elo: " .$_SESSION['noPowerupsElo']. "<br />";
			?>
			
			<a href="skripte/logout.php">Log Out</a>
		</div>

		<div id="naslov">
			<!--<img id="naslov" src="slike/pozadine/CTRLogo.png">-->
		</div>

		<div id="mainSajt">
			<p>LIGE</p>
			<div class="liga">
				<div class="imeLige">
					<h2>ALL PICK</h2>
				</div>
				<table class="tabelaLige">
					<?php include "allPickLiga.php"; ?>
				</table>
			</div>
			<div class="liga">
				<div class="imeLige">
					<h2>NO POWERUPS</h2>
				</div>
				<table class="tabelaLige">
					<?php include "noPowerupsLiga.php"; ?>
				</table>
			</div>

			<div class="liga">
				<div class="imeLige">
					<h2>LOCKED PICK</h2>
				</div>
				<table class="tabelaLige">
					<?php include "lockedPickLiga.php"; ?>
				</table>
			</div>
		</div>

		<div id="topIgraci">
			<div id="topIgraciMapSelect">
				<p>TIME TRIAL</p>
				<select>
					<option value="papusPyramid">Papu's Pyramid</option>
					<option value="sewerSpeedway">Sewer Speedway</option>
					<option value="polarPass">Polar Pass</option>
					<option value="nGinLabs">N. Gin Labs</option>
					<option value="crashCove">Crash Cove</option>
					<option value="roosTubes">Roo's Tubes</option>
					<option value="mysteryCaves">Mystery Caves</option>
					<option value="slideColiseum">Slide Coliseum</option>
					<option value="turboTrack">Turbo Track</option>
					<option value="cocoPark">Coco Park</option>
					<option value="tigerTemple">Tiger Temple</option>
					<option value="dingoCanyon">Dingo Canyon</option>
					<option value="blizzardBluff">Blizzard Bluff</option>
					<option value="dragonMines">Dragon Mines</option>
					<option value="tinyArena">Tiny Arena</option>
					<option value="cortexCastle">Cortex Castle</option>
					<option value="hotAirSkyway">Hot Air Skyway</option>
					<option value="oxideStation">Oxide Station</option>
				</select>
			</div>

			<!--<table id="tabelaTopIgraca">
				<?php include 'papusPyramidTime.php'; ?>
			</table>-->

			<iframe src="vreme/papusPyramidTime.php" frameborder="0" scrolling="no" height="252" width="238">
				
			</iframe>
		</div>

		<div id="eventovi">
			<!--<table id="tabelaEventova">
				<tr>
					<td>13/03</td>
					<td>AP</td>
					<td>20:30</td>
					<td>-</td>
					<td>VILLA</td>
				</tr>

				<tr>
					<td>15/03</td>
					<td>NO</td>
					<td>12:50</td>
					<td>-</td>
					<td>HAUS</td>
				</tr>

				<tr>
					<td>22/03</td>
					<td>LP</td>
					<td>22:00</td>
					<td>-</td>
					<td>HAUS</td>
				</tr>

				<tr>
					<td>31/03</td>
					<td>AP</td>
					<td>15:30</td>
					<td>-</td>
					<td>VILLA</td>
				</tr>

				<tr>
					<td>04/04</td>
					<td>AP</td>
					<td>19:30</td>
					<td>-</td>
					<td>STEVA</td>
				</tr>
			</table>-->
			<p>COMING SOON!</p>
		</div>

		<div id="informacije">
			<p>COMING SOON!</p>
		</div>
	</body>

	<div id="profil">
		<div id="profilModal">
			<span id="profilModalCloseButton" style="font-family: 'crash'; margin-right: 15px;">x</span>
			<iframe id="modalIframe" name="modalIframe" src="" width="584" height="500" frameborder="0"></iframe>
		</div>
	</div>

	<script type="text/javascript">
		$("select").change(function() {
			//alert( "Handler for .change() called." );
			var mapa = this.value;
			var time = "Time.php";
			var lokacija = "vreme/";
			mapa = lokacija.concat(mapa)
			mapa = mapa.concat(time);
			//alert(mapa);
			$("iframe").attr("src", mapa);
		});

		$(document).ready(function() {
		 	$(".ligaIgracIme").click(function() {
		 		$("#profil").fadeIn(800);
		 	});
		});

		function profilModal() {
			$("#profil").fadeIn(800);
		};

		$("#profilModalCloseButton").click(function() {
		 	$("#profil").fadeOut(500);
		});
	</script>
</html>