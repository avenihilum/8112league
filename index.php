<!DOCTYPE html>
<html>
	<head>
		<title>8112</title>
		<link rel="stylesheet" type="text/css" href="css/index.css" />
		<script src="https://code.jquery.com/jquery-3.1.1.js" integrity="sha256-16cdPddA6VdVInumRGo6IbivbERE8p7CQR3HzTBuELA=" crossorigin="anonymous"></script>
	</head>

	<?php include('skripte/loginStatus.php'); ?>

	<body>
		<h1>Uloguj se:</h1>

		<div id="logIn">
			<form action="skripte/logIn.php" method="POST">
				<input type="text" name="ime" placeholder="Unesi korisnicko ime ovde." /><br />
				<input type="password" name="lozinka" placeholder="Unesi lozinku ovde." /><br />
				<input class="submit" type="submit" name="submit" />
			</form>
		</div>

		<h2>Napravi Nalog:</h2>

		<div id="createAccount">
			<form action="skripte/pravljenjeNaloga.php" method="POST" id="pravljenjeNaloga">
				<input type="text" name="ime" placeholder="Unesi korisnicko ime ovde." /><br />
				<input type="password" name="lozinka" placeholder="Unesi lozinku ovde." /><br />
				<input type="password" name="lozinkaProvera" placeholder="Unesi lozinku ovde." /><br />
				<select name="omiljeniChar" form="pravljenjeNaloga">
					<option value="" disabled selected hidden>Odaberi omiljenog karaktera</option>
					<option value="slike/karakteri/crashLogo.png">Crash Bandicoot</option>
					<option value="slike/karakteri/cocoLogo.png">Coco Bandicoot</option>
					<option value="slike/karakteri/neoCortexLogo.png">Doctor Neo Cortex</option>
					<option value="slike/karakteri/tinyTigerLogo.png">Tiny Tiger</option>
					<option value="slike/karakteri/nGinLogo.png">N. Gin</option>
					<option value="slike/karakteri/dingodileLogo.png">Dingodile</option>
					<option value="slike/karakteri/polarLogo.png">Polar</option>
					<option value="slike/karakteri/puraLogo.png">Pura</option>
					<option value="slike/karakteri/ripperRooLogo.png">Ripper Roo</option>
					<option value="slike/karakteri/papuPapuLogo.png">Papu Papu</option>
					<option value="slike/karakteri/komodoJoeLogo.png">Komodo Joe</option>
					<option value="slike/karakteri/pinstripeLogo.png">Pinstripe</option>
					<option value="slike/karakteri/fakeCrashLogo.png">Fake Crash</option>
					<option value="slike/karakteri/nTropyLogo.png">N. Tropy</option>
					<option value="slike/karakteri/pentaPenguinLogo.png">Penta Penguin</option>
				</select><br />
				<input class="submit" type="submit" value="Napravi nalog" />
			</form>
		</div>
	</body>

	<script type="text/javascript">
		$('document').ready(function() {
			$('h2').click(function() {
				$('#createAccount').fadeToggle(500);
			});
		});
	</script>
</html>