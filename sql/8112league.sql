-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 18, 2017 at 05:20 PM
-- Server version: 10.1.19-MariaDB
-- PHP Version: 7.0.13

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `8112league`
--

-- --------------------------------------------------------

--
-- Table structure for table `igraci`
--

CREATE TABLE `igraci` (
  `id` int(11) NOT NULL,
  `ime` varchar(40) NOT NULL,
  `lozinka` varchar(60) NOT NULL,
  `omiljeniChar` varchar(220) NOT NULL,
  `allPickElo` int(4) NOT NULL DEFAULT '1000',
  `lockedPickElo` int(4) NOT NULL DEFAULT '1000',
  `noPowerupsElo` int(4) NOT NULL DEFAULT '1000',
  `papusPyramid` varchar(8) NOT NULL DEFAULT '9:59:59',
  `sewerSpeedway` varchar(8) NOT NULL DEFAULT '9:59:59',
  `polarPass` varchar(8) NOT NULL DEFAULT '9:59:59',
  `nGinLabs` varchar(8) NOT NULL DEFAULT '9:59:59',
  `crashCove` varchar(8) NOT NULL DEFAULT '9:59:59',
  `roosTubes` varchar(8) NOT NULL DEFAULT '9:59:59',
  `mysteryCaves` varchar(8) NOT NULL DEFAULT '9:59:59',
  `slideColiseum` varchar(8) NOT NULL DEFAULT '9:59:59',
  `turboTrack` varchar(8) NOT NULL DEFAULT '9:59:59',
  `cocoPark` varchar(8) NOT NULL DEFAULT '9:59:59',
  `tigerTemple` varchar(8) NOT NULL DEFAULT '9:59:59',
  `dingoCanyon` varchar(8) NOT NULL DEFAULT '9:59:59',
  `blizzardBluff` varchar(8) NOT NULL DEFAULT '9:59:59',
  `dragonMines` varchar(8) NOT NULL DEFAULT '9:59:59',
  `tinyArena` varchar(8) NOT NULL DEFAULT '9:59:59',
  `cortexCastle` varchar(8) NOT NULL DEFAULT '9:59:59',
  `hotAirSkyway` varchar(8) NOT NULL DEFAULT '9:59:59',
  `oxideStation` varchar(8) NOT NULL DEFAULT '9:59:59'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `igraci`
--

INSERT INTO `igraci` (`id`, `ime`, `lozinka`, `omiljeniChar`, `allPickElo`, `lockedPickElo`, `noPowerupsElo`, `papusPyramid`, `sewerSpeedway`, `polarPass`, `nGinLabs`, `crashCove`, `roosTubes`, `mysteryCaves`, `slideColiseum`, `turboTrack`, `cocoPark`, `tigerTemple`, `dingoCanyon`, `blizzardBluff`, `dragonMines`, `tinyArena`, `cortexCastle`, `hotAirSkyway`, `oxideStation`) VALUES
(108, 'NemzaX', '$2y$10$oCGzlegR1ZqKIWXf35upN.iXJKaXb/PTcDrC0f4Ya1ykG4Q51gKpe', 'slike/karakteri/cocoLogo.png', 1029, 938, 787, '1:31:15', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(109, 'Duxan', '$2y$10$SOfq3BuKfd5xqI2bgMkXWOO2322m2F3xMqbJxqkE4ddOZLM6.XV7O', 'slike/karakteri/tinyTigerLogo.png', 955, 1029, 1014, '9:59:59', '2:14:54', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(110, 'Cubri', '$2y$10$hlyAI0snPThSboQi5bpB/e8iaQAGr1LT0A9Wjcz7hupHL.qsVEx.y', 'slike/karakteri/dingodileLogo.png', 1015, 1000, 953, '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(111, 'CiceX', '$2y$10$YK79ZlRdMiiQibbm6EP9YeZ4BRcX61jrnjxKCItla5csvBjpzrq3C', 'slike/karakteri/crashLogo.png', 985, 1000, 937, '9:59:59', '2:17:38', '9:59:59', '9:59:59', '9:59:59', '2:05:08', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(112, 'DjoxiCar', '$2y$10$FArCG5PMjzf/ydZN2MgC6udoVF43.zFwcavEEUI3A/stzYR0yhkDG', 'slike/karakteri/pinstripeLogo.png', 912, 984, 1027, '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(113, 'Boriska', '$2y$10$Jlr6DKYPcTLc1Q17gxesnOc9yil5e0owgq25FqKAHQW6tz4k7JvbW', 'slike/karakteri/polarLogo.png', 970, 1000, 969, '9:59:59', '2:53:38', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(114, 'Bana', '$2y$10$CFobfA/S0JDV6R0EWPxXEOtVegvZSmx6NOKO4ICKkJxHmsIIP4gCu', 'slike/karakteri/neoCortexLogo.png', 1000, 970, 967, '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59'),
(115, 'Yung', '$2y$10$hZaWQ76CiWzzzK9tGS3ZAeLXSei9uIqgRlB3Pjiw0obPZa.mEHjBe', 'slike/karakteri/nGinLogo.png', 1015, 912, 1000, '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59', '9:59:59');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `igraci`
--
ALTER TABLE `igraci`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `igraci`
--
ALTER TABLE `igraci`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=116;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
