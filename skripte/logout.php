<?php
	session_start();
	$_SESSION = array();
	session_destroy();
	session_commit();
	header("Location: ../index.php");
?>

<html>
	<title>Loading...</title>
</html>